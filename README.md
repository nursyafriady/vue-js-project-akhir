# PROJECT AKHIR VUE JS
Tema : Blogs Millenials

# Fitur
1. User : Login, Authentikasi, Register, Logout
2. Halaman Admin : CRUD Blog dan Upload Photo 
3. Halaman Admin, tidak bisa diakses jika tidak login, redirect ke halaman login
4. Setelah success register, redirect ke halaman login


# ANGGOTA
1. Nursyafriady
2. Mohammed Farrell Eghar Syahputra
3. Dzikri Al-Kautsar Sinatria Aryunaputra

# Link Deploy Aplikasi (heroku)
https://blogger-story.herokuapp.com/

# Link Video Demo Aplikasi (youtube)
https://www.youtube.com/watch?v=CBgXEU3IFIc

# Link Screenshot Fitur Aplikasi (gDrive)
https://drive.google.com/drive/folders/19brQNNIkeRWBiKlFbM03wsiRF4DdYt9M?usp=sharing

# Template
bootstrap 4.5, loading UI, vue-toasted, sweetalert, free template course master yang sudah dimodifikasi

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
