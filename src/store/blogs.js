import axios from 'axios'
import swal from 'sweetalert';

export default {
    namespaced : true,
    state: {
        blogs       : [],
        title       : '',
        description : '',
        id          : null,
        photo       : null,
        loading     : false,
        status      : 'CREATE',
        apiDomain   : 'http://demo-api-vue.sanbercloud.com',
        page        : 1,
        cards       : 0,
        perPage     : 0,
    },
    
    mutations: {
        editBlog    : (state, payload) => {
            state.status = 'UPDATE'
            state.title = payload.title
            state.description = payload.description
            state.id = payload.id
        },
        editPhoto   : (state, payload) => {
            state.status = 'UPLOAD'
            state.title = payload.title
            state.description = payload.description
            state.id = payload.id
        },
        changePhoto : (state, payload) => {
            state.photo = payload.target.files[0]
        },
        updateTitle : (state, payload) => {
            state.title = payload
        },
        updateDesc  : (state, payload) => {
            state.description = payload
        },
        changePage  : (state, payload) => {
            state.page = payload
        },
        clearForm   : (state) => {
            state.status = 'CREATE'
            state.title = ''
            state.description = ''
            state.id = null
            state.photo = null
            state.loading = false
        }
    },

    actions: {
        deleteBlog  : (context, payload) => {
            let confirm = window.confirm('Apakah anda yakin menghapus blog ??')

            if (confirm == true) {
                const config = {
                    method  : 'post',
                    url     : `http://demo-api-vue.sanbercloud.com/api/v2/blog/${payload.id}`,
                    params  : { _method : 'DELETE' }
                }
    
                axios(config)
                    .then( (response) => {
                        context.dispatch('getBlogs')
                        swal({
                            title   : response.data.message + " Deleting Blog",
                            button  : 'Close',
                            timer   : 3000
                        })
                    })
                    .catch( (error) => {
                        console.log(error)
                    })
            }
        },
        changePage  : (context, payload) => {
            context.commit('changePage', payload)
            context.dispatch('pagination')
        },
        submitForm  : (context) => {
            context.state.loading = true

            let formData = new FormData()
            formData.append('title', context.state.title)
            formData.append('description', context.state.description)

            const config = {
                method  : 'post',
                url     : 'http://demo-api-vue.sanbercloud.com/api/v2/blog',
                data    : formData
            }

            axios(config)
                .then( (response) => {
                    context.commit('clearForm')
                    context.dispatch('getBlogs')
                    swal({
                        title   : response.data.message + " Add New Blog",
                        button  : 'Close',
                        timer   : 3000
                    })
                })
                .catch ( (error) => {
                    context.commit('clearForm')
                    console.log(error)
                })
        },
        updateBlog  : (context) => {
            context.state.loading = true

            let formData = new FormData()
            formData.append('title', context.state.title)
            formData.append('description', context.state.description)

            const config = {
                method  : 'post',
                url     : `http://demo-api-vue.sanbercloud.com/api/v2/blog/${context.state.id}`,
                data    : formData,
                params  : { _method : 'PUT' }
            }

            axios(config)
                .then( (response) => {
                    context.commit('clearForm')
                    context.dispatch('getBlogs')
                    swal({
                        title   : response.data.message + " Updating Blog",
                        button  : 'Close',
                        timer   : 3000
                    })
                })
                .catch ( (error) => {
                    context.commit('clearForm')
                    console.log(error)
                })
        },
        uploadPhoto : (context) => {
            context.state.loading = true

            let formData = new FormData()
            formData.append('photo', context.state.photo)

            const config = {
                method  : 'post',
                url     : `http://demo-api-vue.sanbercloud.com/api/v2/blog/${context.state.id}/upload-photo`,
                data    : formData,
            }

            axios(config)
                .then( (response) => {
                    context.commit('clearForm')
                    context.dispatch('getBlogs')
                    swal({
                        title   : response.data.message + " Uploading Photo",
                        button  : 'Close',
                        timer   : 3000
                    })
                })
                .catch ( (error) => {
                    context.commit('clearForm')
                    console.log(error)
                })
        },
        getBlogs    : (context) => {
            const config = {
                method: 'get',
                url : context.state.apiDomain + '/api/v2/blog/random/50'
            }

            axios(config)      
                .then( (response) => {
                    let { blogs } = response.data;
                    context.state.blogs = blogs;
                })            
                .catch( (error) => {
                    console.log(error);
                })
        },
        pagination  : (context) => {
            context.state.loading = true

            const config = {
                method: 'get',
                url: context.state.apiDomain + "/api/v2/blog",
                params : { 'page' : context.state.page }
            }
            
            axios(config) 
                .then(response => {
                    let { blogs } = response.data
                    context.state.blogs = blogs.data
                    context.state.page = blogs.current_page
                    context.state.cards = blogs.total
                    context.state.perPage = blogs.per_page
                    context.state.loading = false
                    // this.$toasted.show('Berhasil Menampilkan Semua Berita', {
                    //     type:'success',
                    //     duration: 2000,
                    // })
                })
                .catch( (error) => {
                    console.log(error)
                });
        },
    },
    
    getters : {
        blogs       : state => state.blogs,
        loading     : state => state.loading,
        status      : state => state.status,
        apiDomain   : state => state.apiDomain,
        title       : state => state.title,
        description : state => state.description,
        page        : state => state.page,
        cards       : state => state.cards,
        perPage     : state => state.perPage
    }
}